package com.persnicketysoft.him

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.navigation.fragment.findNavController
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {
    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        auth = Firebase.auth
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var passwordField: EditText = view.findViewById(R.id.editTextTextPassword)
        var emailField: EditText = view.findViewById(R.id.editTextTextEmailAddress)
        view.findViewById<Button>(R.id.button_second).setOnClickListener {
            var password: String = passwordField.text.toString()
            var email: String = emailField.text.toString()
            if(email == null || password == null || email.isEmpty() || password.isEmpty()) {
                Log.d("Invalid","Invalid email or password")
            } else {
                var signer: Task<AuthResult> = auth.signInWithEmailAndPassword(email, password)
                signer.addOnSuccessListener {
                    Log.d("SignInSuccessTag", "Success! Result = ${signer.result.toString()}")
                    findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
                }
                signer.addOnFailureListener {
                    Log.d("SignInFailedTag", "Failure! Result = ${signer.result.toString()}")
                }

            }

        }
    }
}